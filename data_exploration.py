import datetime
import string
from typing import List, Tuple

import langdetect
import json
import os

import nltk
import sklearn
from nltk.corpus import stopwords

from nltk.stem import PorterStemmer
from nltk import word_tokenize
from sklearn.feature_extraction.text import TfidfVectorizer


path = './data/biorxiv_medrxiv/pdf_json/'


class Paper:

    title: str
    abstract: str
    abstract_processed: str
    body: str
    detected_language: str

    def __init__(self, path: str):
        with open(path, 'r') as j:
            parsed_json = json.load(j)
        self.title = parsed_json['metadata']['title']
        self.abstract = " ".join([abstract['text'] for abstract in parsed_json['abstract']])
        self.body = " ".join([body['text'] for body in parsed_json['body_text']])
        self.detected_language = langdetect.detect(self.abstract + " " + self.body)

    def is_english(self) -> bool:
        return self.detected_language == 'en'

    def perform_pre_processing(self):
        self.abstract_processed = \
            TfidfVectorizer(stop_words=stopwords.words('english') + list(string.punctuation)).fit_transform(self.abstract)


def extract_from_json(file_handle):
    parsed_json = json.load(file_handle)
    file_handle.close()
    return \
        " ".join([abstract['text'] for abstract in parsed_json['abstract']]), \
        " ".join([body['text'] for body in parsed_json['body_text']])


def load_basic_data() -> Tuple[List[str], List[str]]:
    file_names = os.listdir(path)
    file_handles = map(lambda n: open(path + n, 'r'), file_names)
    data = map(extract_from_json, file_handles)
    return tuple(zip(*data))


def load_data() -> List[Paper]:
    file_names = os.listdir(path)[: 50]
    return [Paper(path + file_name) for file_name in file_names]


def stemming_tokenizer(text):
    stemmer = PorterStemmer()
    return [stemmer.stem(w) for w in word_tokenize(text)]


def main():
    start_time = datetime.datetime.now()
    abstracts, text_bodies = load_basic_data()
    print("loading done:", datetime.datetime.now() - start_time)

    tfidf_vectorizer = TfidfVectorizer(stop_words=stopwords.words('english'), ngram_range=(2,2))
    abstract_tfidf = tfidf_vectorizer.fit_transform(abstracts)
    print("all done:", datetime.datetime.now() - start_time)

    print(abstract_tfidf.shape)


if __name__ == '__main__':
    main()
