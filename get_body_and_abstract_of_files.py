from most_similar_papers_via_tfidf import extract_data_from_single_json


def main():
    names = ['data/noncomm_use_subset/pdf_json\\022abf5f202c04e2bbf88d829db9844746e0fb71.json', 'data/noncomm_use_subset/pdf_json\\1999185dad27d52562c1dfe77bb72f06bdaf5084.json', 'data/noncomm_use_subset/pdf_json\\2dfdbf2d6b77426866feaf93486327d372fd27c7.json', 'data/noncomm_use_subset/pdf_json\\7ed89e5691a56b6757c21984c3bbb733b5a2dfdc.json', 'data/noncomm_use_subset/pdf_json\\817a885e9613363e08ef920a9e5826a4cb9e1c1e.json', 'data/noncomm_use_subset/pdf_json\\839df627ece5b5fc7bb1ce3b4f96127677fd0494.json', 'data/noncomm_use_subset/pdf_json\\9d36c9a5c87380ec6bb1cee77ced91fd6265d343.json', 'data/noncomm_use_subset/pdf_json\\b3c71d9d7dd9758f8328933f47d7d460bf24c98e.json', 'data/noncomm_use_subset/pdf_json\\b41638f869301e2af9eef7913301d55516fcf4ce.json', 'data/noncomm_use_subset/pdf_json\\be179ef4eaf04a9b155ac385eeabc06620a791b2.json', 'data/noncomm_use_subset/pdf_json\\cb690769762bb2fc4b4d9b898b03623b589fe8c1.json', 'data/noncomm_use_subset/pdf_json\\ea08d7bb1c95436e9ed7af4ed5419cc8fc56e7b7.json', 'data/noncomm_use_subset/pdf_json\\f0e75f3b4aeda66ce88ca4a58a785c8fdf32b9ab.json', 'data/noncomm_use_subset/pdf_json\\f5faed882955e964aa9ea7ac455746bcfec521ba.json']
    for name in names:
        paper_id, _, abstract, body = extract_data_from_single_json(name)
        print(paper_id)
        print(abstract)
        print(body)
        print('\n')


if __name__ == '__main__':
    main()
