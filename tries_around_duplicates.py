import json
import os
from typing import List, Dict, Tuple

from most_similar_papers_via_tfidf import extract_data_from_single_json


def get_data_in_folder(path: str, dict_of_titles: Dict[str, List[str]]) -> Dict[str, List[str]]:
    print(path)
    file_names = os.listdir(path)
    file_names = map(lambda n: os.path.join(path, n), file_names)

    for file_name in file_names:
        with open(file_name, 'r') as file_handle:
            parsed_json = json.load(file_handle)
        title = parsed_json['metadata']['title']
        if title in dict_of_titles:
            dict_of_titles[title].append(file_name)
        else:
            dict_of_titles[title] = [file_name]

    return dict_of_titles


def get_all_data() -> Dict[str, List[str]]:
    all_paths = [
        'data/biorxiv_medrxiv/pdf_json',
        'data/comm_use_subset/pdf_json',
        'data/comm_use_subset/pmc_json',
        'data/custom_license/pdf_json',
        'data/custom_license/pmc_json',
        'data/noncomm_use_subset/pdf_json',
        'data/noncomm_use_subset/pmc_json'
    ]

    dict_of_titles: Dict[str, List[str]] = {}
    for path in all_paths:
        dict_of_titles = get_data_in_folder(path, dict_of_titles)

    return dict_of_titles


def count_the_number_of_deep_duplicates(dict_of_titles: Dict[str, List[str]]):
    asdf = get_info_of_duplicates(dict_of_titles)

    total_diff_bodies = 0
    total_diff = 0
    for title, file_names, _ in asdf:
        qwer = map(extract_data_from_single_json, file_names)
        _, _, abstracts, bodies = tuple(zip(*qwer))
        first = abstracts[0] + " " + bodies[0]
        total_diff += 0 if all(first == i for i in map(" ".join, zip(abstracts[1:], bodies[1:]))) else 1
        total_diff_bodies += 0 if all(bodies[0] == i for i in bodies[1:]) else 1

    print("{} of {} titles with multiple files different bodies".format(total_diff_bodies, len(asdf)))
    print("{} of {} titles with multiple files have different abstract + body".format(total_diff, len(asdf)))


def get_info_of_duplicates(dict_of_titles: Dict[str, List[str]]) -> List[Tuple[str, List[str], int]]:
    asdf = map(lambda t: (t[0], t[1], len(t[1])), dict_of_titles.items())
    asdf = list(filter(lambda t: t[2] > 1, asdf))
    return asdf


def how_many_papers_have_duplicates_and_latex_information(dict_of_titles: Dict[str, List[str]]):
    asdf = get_info_of_duplicates(dict_of_titles)

    qwer = map(lambda x: x[1], asdf)
    qwer = sum(qwer, [])

    total_using_latex = 0
    for file_name in qwer:
        _, _, abstract, body = extract_data_from_single_json(file_name)
        text = abstract + ' ' + body
        total_using_latex += 1 if '\\usepackage' in text else 0

    print('{} have latex information in there'.format(total_using_latex))


def main():
    if False:
        dict_of_titles = get_all_data()
        with open('title_information.json', 'w', encoding='utf-8') as f:
            json.dump(dict_of_titles, f, ensure_ascii=False, indent=4)
    else:
        with open('title_information.json', 'r', encoding='utf-8') as f:
            dict_of_titles = json.load(f)

    how_many_papers_have_duplicates_and_latex_information(dict_of_titles)

    # count_the_number_of_deep_duplicates(dict_of_titles)

    print('hello')


if __name__ == '__main__':
    main()
