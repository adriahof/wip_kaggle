import csv
import json
import os
from typing import Tuple, List, Iterator

import langdetect
import numpy as np
from nltk.corpus import stopwords
from scipy import sparse
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity


def extract_data_from_single_json(file_name: str) -> Tuple[str, str, str, str]:
    with open(file_name, 'r', encoding='utf-8') as file_handle:
        parsed_json = json.load(file_handle)

    paper_id, title, abstract, body = '', '', '', ''

    if 'paper_id' in parsed_json:
        paper_id = parsed_json['paper_id']

    if 'metadata' in parsed_json and 'title' in parsed_json['metadata']:
        title = parsed_json['metadata']['title']

    if 'abstract' in parsed_json:
        abstract = " ".join([abstract['text'] for abstract in parsed_json['abstract']])

    if 'body_text' in parsed_json:
        body = " ".join([body['text'] for body in parsed_json['body_text']])

    # some papers are parsed directly from Latex and have a lot of noise -> filter those
    if '\\usepackage' in abstract or '\\usepackage' in body:
        paper_id, title, abstract, body = '', '', '', ''

    # remove really short papers
    if len(abstract) + len(body) < 100:
        paper_id, title, abstract, body = '', '', '', ''

    return paper_id, title, abstract, body


def get_data_in_one_folder(path: str, filter_non_english: bool = True) \
        -> Tuple[List[str], List[str], List[str], List[str]]:
    file_names = os.listdir(path)
    file_names = map(lambda n: os.path.join(path, n), file_names)
    data = map(extract_data_from_single_json, file_names)
    data = filter(lambda t: t[0].strip() != '', data)  # check if the paper_id is empty <=> something happened during reading
    if filter_non_english:
        data = filter(lambda t: langdetect.detect(" ".join(t[2:])) == 'en', data)
    return tuple(zip(*data))


def get_all_data(filter_non_english: bool = True) -> Tuple[List[str], List[str], List[str], List[str]]:
    all_paths = [
        'data/biorxiv_medrxiv/pdf_json',
        # 'data/comm_use_subset/pdf_json',
        # 'data/comm_use_subset/pmc_json',
        # 'data/custom_license/pdf_json',
        # 'data/custom_license/pmc_json',
        # 'data/noncomm_use_subset/pdf_json',
        # 'data/noncomm_use_subset/pmc_json'
    ]
    all_paper_ids, all_titles, all_abstracts, all_bodies = [], [], [], []
    for path in all_paths:
        i, t, a, b = get_data_in_one_folder(path, filter_non_english)
        all_paper_ids += i
        all_titles += t
        all_abstracts += a
        all_bodies += b
        print("{} has been processed".format(path))
    return all_paper_ids, all_titles, all_abstracts, all_bodies


def get_tfidf_matrix(raw_data):
    return TfidfVectorizer(stop_words=stopwords.words('english'), max_df=0.5, min_df=5).fit_transform(raw_data)


def get_indices_with_highest_similarity(query_tfidf_vector, tfidf_matrix, k: int = 5) -> List[Tuple[float, int]]:
    cosine_similarities = cosine_similarity(tfidf_matrix, query_tfidf_vector).reshape(-1)
    top_indices = np.argpartition(cosine_similarities, -k - 1)[-k - 1:]
    indices_with_highest_similarity = [(cosine_similarities[i], i) for i in top_indices]
    indices_with_highest_similarity.sort(reverse=True)
    return indices_with_highest_similarity[1:]


def main():
    csv_file_name = 'ids_and_titles.csv'
    tfidf_matrix_file_name = 'tfidf_matrix.npz'

    if tfidf_matrix_file_name not in os.listdir('./') or csv_file_name not in os.listdir('./'):
        paper_ids, titles, abstracts, bodies = get_all_data(filter_non_english=True)
        tfidf_matrix = get_tfidf_matrix([a + " " + b for a, b in zip(abstracts, bodies)])
        # save important data for future runs
        with open(csv_file_name, 'w', encoding='utf-8') as f:
            wr = csv.writer(f)
            wr.writerow(paper_ids)
            wr.writerow(titles)
        sparse.save_npz(tfidf_matrix_file_name, tfidf_matrix)
    else:
        tfidf_matrix = sparse.load_npz(tfidf_matrix_file_name)
        with open(csv_file_name, 'r', encoding='utf-8') as f:
            wr = csv.reader(f)
            paper_ids = next(wr)
            next(wr)
            titles = next(wr)

    print("Shape of tfidf matrix: {}".format(tfidf_matrix.shape))
    print("Anzahl        Titel: {}".format(len(titles)))
    print("Anzahl unique Titel: {}".format(len(set(titles))))

    while True:
        query = input("\nEnter the name of your paper-id: ")
        index = -1 if query not in paper_ids else paper_ids.index(query)
        if index < 0:
            print("The entered paper-id isn't in our archive.")
            continue
        best_papers_scores_and_indices = get_indices_with_highest_similarity(tfidf_matrix[index], tfidf_matrix, k=5)
        print("The most similar papers are:")
        for score, index in best_papers_scores_and_indices:
            print("{}\t(title: {}\tsimilarity score: {})".format(paper_ids[index], titles[index], score))


if __name__ == '__main__':
    main()
